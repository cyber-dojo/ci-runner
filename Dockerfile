FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ARG KOSLI_CLI_VERSION=2.11.8
ARG SNYK_VERSION=1.1294.2
ARG TERRAFORM_VERSION=1.6.3
ARG TF_SH_VERSION=0.2.1

# Install dependencies
RUN apt-get update && apt-get install awscli bash curl git jq make ruby sudo unzip --yes

# Install docker
RUN for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh ./get-docker.sh

# Install Kosli
RUN curl -L https://github.com/kosli-dev/cli/releases/download/v${KOSLI_CLI_VERSION}/kosli_${KOSLI_CLI_VERSION}_linux_amd64.tar.gz | tar zx \
      && sudo mv kosli /bin/kosli

# Install Linter
RUN gem install rubocop

# Install Snyk
RUN curl -L --compressed https://github.com/snyk/cli/releases/download/v${SNYK_VERSION}/snyk-linux -o snyk \
      && chmod +x ./snyk \
      && sudo mv ./snyk /usr/bin/snyk

# Install terraform
RUN curl "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
      && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
      && mv terraform ./bin/

# Download tf.sh
RUN curl https://releases.fivexl.io/tf/v${TF_SH_VERSION}/tf.sh --output tf.sh \
      && chmod +x ./tf.sh \
      && sudo mv ./tf.sh /bin/tf.sh
