
Git repo with Dockerfile to create a dedicated CI-runner for Gitlab.  
Used in https://gitlab.com/cyber-dojo/creator

To make the image
$ make build

If you get apt errors "invalid signature was encountered"
You likely need to clear you cache. Try
$ docker system prune
and then try again
